function validateForm() {
    var rtValue = document.getElementById("rt").value;
    var rwValue = document.getElementById("rw").value;

    // Regular expression untuk memeriksa apakah nilai RT dan RW hanya terdiri dari angka
    var numPattern = /^\d+$/;

    // Memeriksa apakah RT dan RW hanya terdiri dari angka
    if (!numPattern.test(rtValue)) {
        alert("RT harus berupa angka.");
        return false;
    }

    if (!numPattern.test(rwValue)) {
        alert("RW harus berupa angka.");
        return false;
    }
    
    // Memeriksa apakah bidang nama tidak diisi
    const namaInput = document.getElementById('nama').value;
    if (namaInput.trim() === '') {
        alert('Nama harus diisi.');
        return false;
    }

    // Mendapatkan elemen form dengan ID 'biodataForm'
    const form = document.getElementById('biodataForm');

    // Mendapatkan nilai dari input email
    const email = form['email'].value;

    // Mendapatkan nilai dari input telepon
    const telepon = form['telepon'].value;

    // Mendapatkan nilai dari dropdown status
    const status = form['status'].value;

    // Mendapatkan nilai dari input program studi
    const programStudi = form['programStudi'].value;

    // Mendapatkan nilai dari input universitas
    const universitas = form['universitas'].value;

    // Mendapatkan nilai dari input jumlah saudara
    const jumlahSaudara = form['jumlahSaudara'].value;

    // Mendapatkan nilai dari dropdown golongan darah
    const golonganDarah = form['golonganDarah'].value;

    // Memvalidasi jika status adalah mahasiswa, program studi dan universitas harus diisi
    if (status === 'mahasiswa') {
        if (programStudi === '' || universitas === '') {
            alert('Program Studi dan Universitas harus diisi untuk mahasiswa.');
            return false; // Menghentikan pengiriman form jika validasi gagal
        }
    }

    // Pola validasi untuk email
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/; // Regex untuk validasi email
    if (!emailPattern.test(email)) {
        alert('Alamat email tidak valid.');
        return false; // Menghentikan pengiriman form jika validasi gagal
    }

    // Pola validasi untuk nomor telepon (hanya angka, panjang antara 10 hingga 12 digit)
    const teleponPattern = /^[0-9]{10,12}$/; // Regex untuk validasi nomor telepon
    if (!teleponPattern.test(telepon)) {
        alert('Nomor telepon harus terdiri dari 10-12 digit angka.');
        return false; // Menghentikan pengiriman form jika validasi gagal
    }

    // Validasi untuk jumlah saudara (harus angka positif)
    if (jumlahSaudara < 0 || !Number.isInteger(Number(jumlahSaudara))) {
        alert('Jumlah saudara harus angka positif.');
        return false; // Menghentikan pengiriman form jika validasi gagal
    }

    // Validasi untuk golongan darah (harus dipilih)
    if (!['A', 'B', 'AB', 'O'].includes(golonganDarah)) {
        alert('Golongan darah tidak valid.');
        return false; // Menghentikan pengiriman form jika validasi gagal
    }
    
    // Menampilkan pesan terima kasih
    alert('Terima kasih! Formulir telah berhasil dikirim.');

    // Menghapus elemen formulir dari DOM
    form.remove();

    // Formulir valid
    return true;
}
